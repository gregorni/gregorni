# About me

Hi, my name is Gregor Niehl.

I'm a teenage tech enthusiast from Germany mostly interested in Linux and Open Source.
I've previously coded in Bash, Python and Rust, although I'm open to trying out new languages.

## My apps

- [Letterpress](https://gitlab.gnome.org/World/letterpress)


## My Socials:
- [Matrix](https://matrix.to/#/@gregorni:gnome.org)

## My other Git accounts:
- [GitLab](https://gitlab.com/gregorni)
- [Freedesktop GitLab](https://gitlab.freedesktop.org/gregorni)
- [Codeberg](https://codeberg.org/gregorni)
- [GitHub](https://github.com/gregorni)
