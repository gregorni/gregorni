# About me

Hi, my name is gregorni.

I'm a teenage tech enthusiast from Germany mostly interested in Linux and Open Source.
I've previously coded in Bash, Python and Rust, although I'm open to trying out new languages.

## My apps

- [Letterpress](https://gitlab.gnome.org/World/letterpress)
- [Calligraphy](https://gitlab.gnome.org/World/calligraphy)


## My Socials:
- [Mastodon](https://fosstodon.org/@gregorni)
- [Matrix](https://matrix.to/#/@gregorni:gnome.org)

## My other Git accounts:
- [GitLab](https://gitlab.com/gregorni)
- [GNOME GitLab](https://gitlab.gnome.org/gregorni)
- [Codeberg](https://codeberg.org/gregorni)
- [GitHub](https://github.com/gregorni)
